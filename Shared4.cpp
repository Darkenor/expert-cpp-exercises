#include <memory>
#include <iostream>
#include <assert.h>

//DON'T TOUCH MY WIDGET. :)
class Widget
{
public:
	Widget() : _value(4) {};
	~Widget()
	{
		std::cout<<"Test destroyed."<<std::endl;
	}
	int Value() { return _value;  }
private:
	int _value;
};

int main()
{
	std::shared_ptr<Widget> p = std::make_shared<Widget>();
	std::shared_ptr<Widget> q = p;
	std::cout << "Calling reset";
	p.reset();
	assert(q == nullptr); //...Shouldn't this work? What's wrong above?
	std::puts("done");
	return 0;
}