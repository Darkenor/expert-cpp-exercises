#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

class Klaxxon
{
public:
	Klaxxon() { std::cout << "Created" << std::endl; }
	~Klaxxon() { std::cout << "Destroyed" << std::endl; }
	Klaxxon(Klaxxon const&) { std::cout << "Copied" << std::endl; }
	Klaxxon(Klaxxon&&) { std::cout << "Moved" << std::endl; }
	void Angry() { throw new std::exception("Rawr"); }
};


//Do not touch this function.
std::unique_ptr<Klaxxon> MakeKlaxxon()
{
	return std::make_unique<Klaxxon>();
}


int main()
{
	std::unique_ptr<Klaxxon> ptr = MakeKlaxxon();
	static_assert(std::is_same<decltype(ptr), 
		std::shared_ptr<Klaxxon>>::value, "Nope. Needs to be a shared pointer.");
	return 0;
}