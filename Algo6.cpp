#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

int main()
{
	//Exercise 6
	std::vector<int> nums{ 1,3,2,9,8,6,5,4,7 };
	//magic line goes here!!!
	std::vector<int> answer{ 1,2,3,4,9,8,6,5,7 }; 
	auto result = std::equal(answer.begin(), answer.begin()+4, nums.begin());

	assert(result == true);

	return 1;
}