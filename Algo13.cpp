#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>
#include <numeric>
#include <functional>
#include <cctype>

int main()
{

	//Exercise 13
	std::vector<int> nums{ 1,2,3,66,4,66,5,6,66,7,8,66,9}; 
	std::vector<int> answer{ 1,2,3,4,5,6,7,8,9 };
	//magic line goes here! note: this will (annoyingly) require you calling erase on your vec.
	nums.erase(std::remove(nums.begin(), nums.end(), 99), nums.end());
	assert(nums == answer);

	return 1;
}