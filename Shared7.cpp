#include <iostream>
#include <memory>
#include <assert.h>

/*
This is a fringe case example from the blog I sourced before we started
the exerices. It's in the slides you'll receive. You will find this
behaves....oddly. Your goal is to not get it to do so while not 
modifying main. I did not cover this. You'll need to Google a bit about...
"this"...
*/
struct BrokeASaurus {
	std::shared_ptr<BrokeASaurus> CreateBrokeASaurusRex() {
		return std::shared_ptr<BrokeASaurus>(this);  
	}
};

int main() {
	auto whoa = std::make_shared<BrokeASaurus>();
	auto tmp = whoa->CreateBrokeASaurusRex();
} 