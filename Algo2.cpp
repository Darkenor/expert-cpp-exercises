#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

int main()
{
	//Exercise 2
	std::vector<int> nums { 3, 2, 10, 45, 33, 56, 23, 47 };
	//magic line goes here!!!
	std::vector<int> answer{ 2, 3, 10, 45, 33, 56, 23, 47 };
	assert(nums == answer);


	return 1;
}