#include <iostream>
#include <memory>
#include <assert.h>

//Your goal is to implement the "pimpl" idiom...but using a smart pointer!
//Google 'pimpl' if you are unfamiliar with the term.

class CarImpl
{
public:
	std::string run()
	{
		return "runnin";
	}
};

class Car
{
public:
	Car();
	~Car();
	std::string run();
private:
	//pimpl stuff
};


int main()
{
	Car c;
	auto a = c.run();
	assert(a == "runnin");
}