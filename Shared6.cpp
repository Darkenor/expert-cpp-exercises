#include <memory>
#include <iostream>
#include <assert.h>

/*
In this assignment, implement the following six fuction signatures/prototypes 
but not their implementation. The goal is to ensure you understand signature
consequences. When complete, notify me to check that you have it right.
*/
auto nasty_global = std::make_shared<int>(42); // the answer to all things.

void ByRef() //takes int by pointer for a smart pointer
{
}

void ByRawPointer() // takes int by ref for a smart pointer
{
}

void Sink()  //...sink. lol
{
}

void OutParamUnique() //Function will specify that it can modify a unique pointer
{
}

void OutParamShared() //Function will specify that it can modify a shared pointer
{
}

void TakesOwnership() //Function will specify that it takes ownership of a shared ptr.
{
}

int main()
{
	return 0;

}