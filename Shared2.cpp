#include <iostream>

#include <memory>
#include <iostream>

struct B;
struct A {
	std::shared_ptr<B> b;
	~A() { std::cout << "A Destructor" << std::endl; }
};

struct B {
	std::shared_ptr<A> a;
	~B() { std::cout << "B Destructor" << std::endl;  }
};

void useAnB() {
	auto a = std::make_shared<A>();
	auto b = std::make_shared<B>();
	a->b = b;
	b->a = a;
}

int main() 
{
	//...is it a little odd that a destructor isn't called here? Could it be fixed?
	useAnB();
	std::cout << "Finished using A and B\n";
}