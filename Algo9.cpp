#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

int main()
{
	//Exercise 9
	std::vector<int> nums{ 1,2,3,4,5,6,7,8,9 };
	//magic line goes here!!
	std::vector<int> answer{ 2,3,4,5,6,7,8,9,1 };

	assert(nums == answer); 

	return 1;
}