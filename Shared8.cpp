/*
Surprise! Different format!

In this exercise, we are going to ask you to create a sample applcation. 
The only requirement is that the application should show a good use of 
a case where you would possibly need a weak_ptr. The code should be 
relatively obvious and it should make sound, logical sense. The reason
the exercise is being done is this format is two fold:

1. Use cases of weak_ptr are fairly difficult to contrive. The exercise will be fruitful.
2. 3 of you will be chosen at random to present your implementation for comments!

Enjoy and good luck. You have one hour.

Note: If you have trouble thinking of ideas, I will write one on the white board after
ten minutes. But your implementation of it will need to be unique.
*/
