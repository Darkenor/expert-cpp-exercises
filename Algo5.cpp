#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

int main()
{
	//Exercise 5
	std::vector<int> nums{ 1,3,2,9,8,6,5,4,7 };
	//magic line goes here!!!
	std::vector<int> answer{ 1,2,3,4,5,6,7,8,9 }; 
	assert(nums == answer);


	return 1;
}