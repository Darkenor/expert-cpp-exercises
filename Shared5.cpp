#include <iostream>
#include <algorithm>
#include <memory>
using namespace std;

class Widget {};

int main()
{
	Widget* widget = new Widget();

	std::shared_ptr<Widget> sp1(widget);
	cout << sp1.use_count() << endl;

	std::shared_ptr<Widget> sp2(widget);
	cout << sp2.use_count() << endl;

	return 0;
}