#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>

int main()
{
	//Exercise 1
	std::string steve = "Steve is just sooooo cool";
	std::string nope = "dumb";
	//magic line goes here!!
	assert(steve.compare("Steve is just sooooo dumb") == 0);

	return 1;
}