#include <algorithm>
#include <iostream>
#include <string>
#include <assert.h>
#include <vector>
#include <numeric>
#include <functional>

int main()
{

	//Exercise 11
	std::vector<int> set_1{ 1,2,3,8,9,10,11}; 
	std::vector<int> set_2{ 1,2,3,6,7,11 }; 
	std::vector<int> answer;
	//magic line goes here!
	std::vector<int> magic{ 8,9,10 };
	assert(answer == magic);

	return 1;
}